/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : xuijee

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2016-05-31 22:31:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_demo
-- ----------------------------
DROP TABLE IF EXISTS `sys_demo`;
CREATE TABLE `sys_demo` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(10) DEFAULT NULL COMMENT '名称',
  `phone` varchar(30) DEFAULT NULL COMMENT '电话',
  `img` varchar(255) DEFAULT NULL COMMENT '头像',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `key_demo_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='crud测试';

-- ----------------------------
-- Records of sys_demo
-- ----------------------------
INSERT INTO `sys_demo` VALUES ('383eadbf329a413b947bbf31748e8cd0', '程旭', '13888888888', null, 'cxmail@qq.com', '1986-12-03', '2016-05-31 22:30:40');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(8) NOT NULL COMMENT '菜单id',
  `pid` varchar(8) DEFAULT NULL COMMENT '父id',
  `name` varchar(10) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(30) DEFAULT NULL COMMENT '链接地址',
  `img` varchar(30) DEFAULT NULL COMMENT '图片',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_menu_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统菜单';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('01', '0', '演示DEMO', '', '', '1');
INSERT INTO `sys_menu` VALUES ('0101', '01', 'CRUD', '/sysDemo', null, '101');
INSERT INTO `sys_menu` VALUES ('09', '0', '系统管理', null, null, '9');
INSERT INTO `sys_menu` VALUES ('0901', '09', '用户管理', '/sysUser', null, '901');
INSERT INTO `sys_menu` VALUES ('0902', '09', '角色管理', '/sysRole', null, '902');
INSERT INTO `sys_menu` VALUES ('0903', '09', '菜单管理', '/sysMenu', null, '903');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(32) NOT NULL COMMENT '角色ID',
  `name` varchar(20) DEFAULT NULL COMMENT '角色名称',
  `home` varchar(50) DEFAULT NULL COMMENT '角色默认主页',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `createTime` datetime DEFAULT NULL COMMENT '录入时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_role_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '管理员', '/sysDemo', '管理员', '2016-03-12 20:35:40');
INSERT INTO `sys_role` VALUES ('2f4109149a9c42aa8f2a9c015be93cfc', 'aa', 'b', 'c', '2016-05-30 23:56:54');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `roleId` varchar(32) NOT NULL COMMENT '角色id',
  `menuId` varchar(8) NOT NULL COMMENT '菜单id',
  `operate` varchar(20) DEFAULT NULL COMMENT '权限',
  PRIMARY KEY (`roleId`,`menuId`),
  KEY `fk_role_menu_menu` (`menuId`),
  CONSTRAINT `sys_role_menu_ibfk_1` FOREIGN KEY (`menuId`) REFERENCES `sys_menu` (`id`),
  CONSTRAINT `sys_role_menu_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES `sys_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '01', null);
INSERT INTO `sys_role_menu` VALUES ('1', '0101', null);
INSERT INTO `sys_role_menu` VALUES ('1', '09', null);
INSERT INTO `sys_role_menu` VALUES ('1', '0901', null);
INSERT INTO `sys_role_menu` VALUES ('1', '0902', null);
INSERT INTO `sys_role_menu` VALUES ('1', '0903', null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `roleId` varchar(32) DEFAULT NULL COMMENT '角色id',
  `username` varchar(20) DEFAULT NULL COMMENT '登陆账号',
  `password` varchar(50) DEFAULT NULL COMMENT '登陆密码',
  `name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `phone` varchar(30) DEFAULT NULL COMMENT '电话',
  `email` varchar(30) DEFAULT NULL COMMENT '电子邮件',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `lastTime` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` int(1) DEFAULT NULL COMMENT '状态 (0禁用、1启用)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_user_id` (`id`) USING BTREE,
  KEY `key_user_roleId` (`roleId`) USING BTREE,
  CONSTRAINT `sys_user_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `sys_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '程旭', '13889492125', 'cxmail@qq.com', '2016-03-06 20:08:12', '2016-05-31 22:24:05', '1');
