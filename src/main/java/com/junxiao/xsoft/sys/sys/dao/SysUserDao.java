package com.junxiao.xsoft.sys.sys.dao;

import com.junxiao.xsoft.base.dao.BaseDao;
import com.junxiao.xsoft.sys.sys.entity.SysUser;

/**
 * 系统用户表dao
 * 
 * @copyright © 2016 大连骏骁网络科技有限公司
 * @author 程旭(cxmail@qq.com)
 * @createDate 2016-01-29
 * @version: V1.0.0
 */
public interface SysUserDao extends BaseDao<SysUser> {

}
