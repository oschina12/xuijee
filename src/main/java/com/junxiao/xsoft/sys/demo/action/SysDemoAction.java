package com.junxiao.xsoft.sys.demo.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.junxiao.xsoft.base.action.BaseAction;
import com.junxiao.xsoft.base.utils.DateUtil;
import com.junxiao.xsoft.base.utils.ToolsUtil;
import com.junxiao.xsoft.sys.demo.biz.SysDemoBiz;
import com.junxiao.xsoft.sys.demo.entity.SysDemo;

/**
 * crud测试action
 * 
 * @copyright © 2016 大连骏骁网络科技有限公司
 * @author 程旭(cxmail@qq.com)
 * @createDate 2016-05-29
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/${adminPath}/sysDemo")
public class SysDemoAction extends BaseAction {

	@Autowired
	private SysDemoBiz sysDemoBiz;

	@RequestMapping(value = "")
	public String index() {
		return "sys/demo/sys_demo";
	}

	/**
	 * 添加
	 */
	@RequestMapping(value = "/saveSysDemo", method = RequestMethod.POST)
	@ResponseBody
	public Object saveSysDemo(HttpServletRequest request, @ModelAttribute SysDemo sysDemo) {
		sysDemo.setId(ToolsUtil.getUUID());
		sysDemo.setCreateTime(DateUtil.getNow());
		sysDemoBiz.save(sysDemo);
		return sendOk();
	}

	/**
	 * 修改
	 */
	@RequestMapping(value = "/updateSysDemo", method = RequestMethod.POST)
	@ResponseBody
	public Object updateSysDemo(HttpServletRequest request, @ModelAttribute SysDemo sysDemo) {
		sysDemoBiz.update(sysDemo);
		return sendOk();
	}

	/**
	 * 删除
	 * 
	 * @param ids
	 *            id字符串多个使用,分隔
	 */
	@RequestMapping(value = "/deleteSysDemo", method = RequestMethod.POST)
	@ResponseBody
	public Object deleteSysDemo(HttpServletRequest request, @RequestParam String ids) {
		sysDemoBiz.delete(ids.split(","));
		return sendOk();
	}

	/**
	 * 分页查询
	 * 
	 * @param request
	 *            HttpServletRequest对象
	 * @param page
	 *            当前页码
	 * @param rows
	 *            每页条数
	 * @param params
	 *            查询参数
	 * @return 返回查询结果json
	 */
	@RequestMapping(value = "/findSysDemo", method = RequestMethod.POST)
	@ResponseBody
	public Object findSysDemo(HttpServletRequest request, int page, int rows, Map<String, Object> params) {
		return sysDemoBiz.queryPage(page, rows, params);
	}

}