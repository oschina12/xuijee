xuijee是一个java快速开发框架

xui、Spring、SpringMVC、Shiro、底层基于jdbcTemplate封装的BaseDao继承就能完成crud基本操作。

代码量少、学习简单、功能强大、轻量级、易扩展.


![登录页面](http://git.oschina.net/uploads/images/2016/0531/222444_26f519d8_418082.png "登录页面")

![系统后台](http://git.oschina.net/uploads/images/2016/0531/223720_8b460b15_418082.png "系统后台")

前端xui地址 [http://git.oschina.net/junxiaosoft/xui](http://git.oschina.net/junxiaosoft/xui)

捐赠

微信：![微信](http://git.oschina.net/uploads/images/2016/0531/225605_67b2c437_418082.png "微信")
支付宝：![支付宝](http://git.oschina.net/uploads/images/2016/0531/225641_77294286_418082.png "支付宝")